---
title: 'Projektdokumentation CO2-Ampel'

author:
    - Pascal Gleiss
    - Lena Hettich 
    - Jiline Höfliger
    - Carolin Schmid
    - Jaqueline Weber
    
date: \today

link-citations: true
toc: true
toc-title: Inhaltsverzeichnis
nocite: '@*'
titlepage: true
toc-own-page: true
titlepage-logo: "images/logo_HSKE.pdf"
logo-width: 130mm
csl: deutsche-gesellschaft-fur-psychologie.csl
---

# Einleitung

“Jeder 50. Luftzug stammt direkt aus der Lunge einer anderen Person.”[^01] 

Dies haben Berechnungen bei einer Kohlendioxidkonzentration von 1.200 ppm im geschlossenen Innenraum ergeben. Diese Erkenntnis verdeutlicht, dass die Luftqualität in Innenräumen berücksichtigt werden sollte, besser noch mithilfe einer CO2-Ampel überwacht werden.[^02]

Gerade im Zusammenhang mit der Coronapandemie wurden vermehrt CO2-Ampeln eingeführt. Diese helfen dabei, die Luftqualität im Raum zu überprüfen und warnen bei zu hohen CO2-Werten, sodass rechtzeitig Lüftungsmaßnahmen ergriffen werden können. 
Durch regelmäßiges Lüften kann einerseits die Ansteckungsgefahr von COVID sowie von weiteren Atemwegserkrankungen reduziert werden. Andererseits zeigen Studien, dass sich Belüftung und eine verbesserte Luftreinigung positiv auf die Konzentrationsfähigkeit sowie kognitive Leistungsfähigkeit von Schüler*innen auswirken und zudem  Fehltage reduziert werden können.[^03]
Auf Grundlage dessen ist eine strategische, auf Daten basierte Lüftung von Innenräume insbesondere in Bildungseinrichtungen von hoher Relevanz, weshalb sich die Gruppe für die Realisierung einer CO2-Ampel entschieden hat.

Mit der Anbringung der CO2-Ampel im Vorlesungsraum ist es das Ziel, allen Studierenden und Dozierenden eine optimale Luftqualität zu bieten, das Krankheitsrisiko zu verringern und das Wohlbefinden beim Lernen zu steigern.

Zukünftig können weitere CO2-Ampeln in geschlossenen Räumen der Hochschule Kehl angebracht werden, um ein umfassendes Monitoring der Luftqualität sicherzustellen.

In dieser Arbeit wird zunächst auf die theoretischen Grundlagen der CO2-Ampel eingegangen. Daraufhin wird die Projektskizze mit dem Vorgehen sowie die Anforderungen an den Sensor geschildert. Im folgenden Kapitel wird der Lötworkshop thematisiert, wobei anhand von Bildern die Vorgehensweise dargestellt wird. Zudem wird die Programmierung des Sensors in der Arduino-Umgebung erläutert. Das letzte Kapitel behandelt die Abbildung der Daten im Dashboard und gibt Aufschluss über die Zusammenhänge der gewonnenen Daten.

[^01]: Umwelt Campus: https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/IoT-Werkstatt/octopus/IoT_MINT_Making_Zukunft_Gestalten.pdf, S. 36 (zuletzt abgerufen am 31.01.2023).
[^02]: Ebd, S.36.
[^03]: The Lancet COVID-19 Commission: https://static1.squarespace.com/static/5ef3652ab722df11fcb2ba5d/t/60a3d1251fcec67243e91119/1621348646314/Safe+Work+TF+Desigining+infectious+disease+resilience+April+2021.pdf, S. 3 (zuletzt abgerufen am 31.01.2023). 

# Theoretische Grundlagen

## CO2-Ampel - Allgemein

LoRaWAN ist ein Low Power Wide Area Network (LPWAN), welches sich aufgrund seiner großen Reichweite, freien Nutzung der Frequenzbänder und geringem Energiebedarf unter anderem gut für Smart City Projekte eignet.[^1]

Für das Projekt verwenden wir das offene LoRaWAN „The Things Network“, welches um ein eigenes Gateway ergänzt wird.[^2] Somit können die Sensoren mit dem Internet verbunden, Daten in einem Dashboard gesammelt, zusammengeführt und analysiert werden.

Insbesondere während der Coronapandemie hat das Thema Luftqualität in Innenräumen einen hohen Stellenwert erhalten. Daher wurden in den letzten Jahren in einigen Bildungseinrichtungen als auch öffentlichen Gebäuden sogenannte CO2-Ampeln installiert, welche die CO2-Konzentration im jeweiligen Raum messen. Dabei hängt die CO2-Konzentration in Innenräumen von mehreren Faktoren ab. Zunächst ist die Anzahl der Personen, welche CO2 im Raum ausstößt, zu berücksichtigen. Zusätzlich sind die Raumgröße, die Zeit, welche im jeweiligen Raum verbracht wird, die Aktivität der Raumnutzenden, die Verbrennungsvorgänge sowie der Luftwechsel essentielle Aspekte, welche die CO2-Konzentration im Innenraum beeinflussen. Insbesondere in Bildungseinrichtungen wie Schulen oder Universitäten befinden sich viele Personen in recht kleinen Räumen über einen längeren Zeitraum, ohne Luftwechsel in einen Klassenraum, wodurch ein schneller CO2-Anstieg eine maßgebliche Folge ist.[^3]

Laut Umweltbundesamt haben mehrere Untersuchungen ergeben, dass große Defizite bezüglich ausreichender Innenraumluftqualität in Schulen bestehen. Daraus wird abgeleitet, dass die Hinweise zum richtigen Lüften in Schulen nicht konsequent realisiert werden. Wofür die Unwissenheit über den Anstieg und Verlauf der CO2-Konzentration in Innenräumen Ursache sein kann.[^4] Diese Erkenntnis kann auch auf die Hochschule Kehl übertragen werden, da gerade im Winter eine geringe Bereitschaft, aufgrund der Kälte besteht zu lüften. 

[^1]: https://www.lora-wan.de/ (zuletzt abgerufen am 31.01.2023).
[^2]: Foliensatz Smart City Lab vom 10.10.2022, S.14.
[^3]: Umweltbundesamt: https://www.umweltbundesamt.de/sites/default/files/medien/pdfs/kohlendioxid_2008.pdf, S. 1-2 (zuletzt abgerufen am 31.01.2023). 
[^4]: Ebd, S. 2.

\pagebreak
## CO2 als Messwert für Luftqualität

In diesem Kapitel wird der Frage auf den Grund gegangen, warum sich CO2 als Messwert für die Luftqualität eignet. Des Weiteren wird auf CO2-Richtwerte eingegangen.

Kohlendioxid (CO2) ist ein farbloses und geruchloses Gas sowie Abbauprodukt des menschlichen Stoffwechsels. Bereits 1858 hat der deutsche Hygieniker Max von Pettenkofer sich mit der CO2-Konzentration in Innenräumen auseinandergesetzt. Er definierte einen maximalen Richtwert von 1000 ppm (parts per million) für den CO2-Gehalt in Räumen. Dieser Anhaltspunkt ist auch als Pettenkofer-Zahl bekannt.[^5]

Außenluft enthält rund 400 ppm CO2, im Vergleich dazu enthält die ausgeatmete Luft eines Menschen recht konstant 40.000 Kohlendioxid-Moleküle auf 1 Million Luftteilchen (ppm). Dies zeigt deutlich, dass der Mensch für eine Luftverunreinigung im Innenraum verantwortlich ist und unterstreicht die Bedeutung von regelmäßigen Lüften bzw. einem Austausch der Luft.[^6]

[^5]: Umwelt Campus: https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/IoT-Werkstatt/octopus/IoT_MINT_Making_Zukunft_Gestalten.pdf, S. 32-33 (zuletzt abgerufen am 31.01.2023).
[^6]: Ebd, S.33.

## Funktionsweise der CO2-Ampel

In diesem Kapitel werden die Grundlagen der Funktionsweise einer CO2-Ampel erläutert. Auf die Sensorik und Programmierung wird in den nachfolgenden Kapiteln  eingegangen.

Die CO2-Ampel wird im Raum 201 des DVM20 angebracht und soll dort die CO2-Konzentration messen und dadurch Aufschluss über die Luftqualität im Raum geben. 

Folgende Tabelle veranschaulicht die Ampelfarben in Abhängigkeit der CO2-Grenzwerte, welche sich an den Leitwerten des Umweltbundesamtes orientieren.[^7]

| **CO2-Wert in ppm** | **Ampelfarbe** |
|---------------------|----------------|
| unter 800           | grün           |
| 800 bis 1.399       | gelb           |
| ab 1.400            | rot            |

Abbildung 1: CO2-Grenzwerte für die CO2-Ampel 

Bei einer CO2-Konzentration von unter 800 ppm kann von einer hohen Raumluftqualität ausgegangen werden und keine weiteren Maßnahmen sind notwendig. Erhöht sich der Gehalt auf einen Wert von 800 und mehr, so leuchtet die Ampel gelb und es kann nur noch von einer mittleren bis mäßigen Raumluftqualität gesprochen werden. Zudem sollten Lüftungsmaßnahmen ergriffen werden. Bei einem Grenzwert ab 1.400 ppm ist die Raumluftqualität niedrig und eine hygienisch auffällige Bewertung der Luft ist gegeben. Spätestens zu diesem Zeitpunkt sollte gelüftet werden, um die CO2-Konzentration im Raum wieder zu senken und eine hohe Raumluftqualität zu erzielen.[^8]

[^7]: Umweltbundesamt: https://www.umweltbundesamt.de/sites/default/files/medien/pdfs/kohlendioxid_2008.pdf, S. 9-11 (zuletzt abgerufen am 31.01.2023).
[^8]: Ebd, S. 9-11.

## Weitere Messwerte und deren Zusammenhänge

Neben dem CO2-Sensor wurde der PAX-Counter hinzugefügt, um einerseits die Anzahl der Personen im Raum zu erfassen und anderseits die Zusammenhänge zwischen Anzahl der Personen und Anstieg der CO2-Konzentration im Innenraum zu analysieren. Wie in Kapitel 2.1 CO2-Ampel – Allgemein bereits beschrieben, ist die Personenanzahl im Raum ein großer Faktor, welcher den CO2-Gehalt beeinflusst. Zudem wird der Einflussfaktor Zeit als Wert im Dashboard abgebildet. Dadurch lassen sich erste Erkenntnisse und Zusammenhänge dieser drei Einflussfaktoren untersuchen.

Darüber hinaus wird mit dem Sensor die Raumtemperatur gemessen. Im Anbetracht der Energiekrise kann durch eine kontinuierliche Temperaturmessung sichergestellt werden, dass eine Raumtemperatur von 19 bis 20 Grad eingehalten wird und keine übermäßigen Ressourcen verschwendet werden.

\pagebreak
# Projektskizze

Nach der theoretischen Einführung durch Herrn Itrich in die Thematik Iot haben wir uns Gedanken darüber gemacht, welches Device für uns in Kehl einen Mehrwert durch seine Daten liefern kann. Hierbei ist die Wahl ziemlich schnell auf einen Co2-Sensor gefallen, der mittels Ampelfunktion angibt, wie wir im Vorlesungssaal unser Lüftungsverhalten steuern sollten. Wir haben hierfür folgende Komponenten verwendet:

* Octopus IoT-Werkstatt
* Bosch BME Umwelt
* Sensirion SCD30 Umwelt
* WS2812 Pixel

**Geplanter Ablauf:**

**24.11.2022: Lötworkshop**  
Die Vorgehensweise beim Lötworkshop kann man dem Kapitel "Lötworkshop" entnehmen.
 
**05.12.2022: Inbetriebnahme**  
Für die erstmalige Inbetriebnahme unseres Devices haben wir den Quickstart Guide der IoT- Werkstatt [^9] zur Hand genommen und so den Sensor mit unserem PC verbunden, um eine Programmierung mittels Arduino/Ardublock zu konzipieren und hierauf hochladen zu können. 
Hier kurz unsere Vorgehensweise anhand des Quickstart-Guides:

* Zip-File der IoT-Werkstatt herunterladen
* USB-Treiber installieren
* PC & Octopus-Board per USB-Kabel miteinander verbinden
* Adruino-Umgebung starten
* COM-Port-auswählen
* Iot-Adrublock starten programmieren starten
* Programm per „Hochladen auf den Arduino“ an den Octopus übertragen
 
Die Anmeldung unseres LoRaWan-Endgerätes in das LoRaWan-Netzwerk wurde bereits durch unseren Dozenten durchgeführt. Wir haben hierzu nur noch einen Key erhalten, den wir einmalig eingeben mussten.
 
**15.12.2022: Programmieren und Einbinden der Sensorik**  
Das Programmieren siehe in Kapitel "Programmierung".
Hier haben wir neben Funktionstests den Pax-Counter und die Temperatur in ArduBlock programmiert. Zudem war es notwendig, die Hardware auszutauschen, um unseren Anforderungen an eine Ampelfunktion gerecht zu werden. Hier gab es einige Anpassungen zu machen.
 
**19.12.2022: Dashboard**  
An diesem Termin haben wir eine Einführung in die Konzipierung eines Dashboards erhalten, mittels dem wir die Daten aus unseren Sensoren in Echtzeit visualisieren können. Weiterführende Informationen hierzu siehe Kapitel "Dashboard".
 
**09.01.2023: Anpassungen**  
Da die Aufputz-Box noch einen lichtundurchlässigen Deckel besaß, war es notwendig, diesen zu optimieren. Hierzu wurde ein Plexiglasfenster eingebaut. Die Box wurde letztendlich mit Klettstreifen im Vorlesungssaal über dem Smartboard, mittig an der Decke aufgehängt, somit knapp auf Deckenhöhe. Ein niedrigeres Anbringen wäre nicht möglich, wegen der fehlenden Stromversorgung auf einer geringen Raumhöhe. Die Empfehlung des Umweltbundesamt ist, „die Geräte werden am besten in Atemhöhe (ca. 1,5 m bei sitzenden Personen) und mittig im Raum
platziert".[^10] Raumtechnisch ist dies in unserem Vorlesungssaal leider nicht realisierbar. 
 
**16.01.2023: LED-Tausch & Tests**  
Es war notwendig, einen größeren LED-Ring mit unseren Sensoren zu verbinden, da die zuvor verbundenen einzelnen LED`s in Verbindung mit der Aufputz-Box nicht das gewünschte Ziel erreicht hätten, eindeutig als Ampel zu fungieren. Nachdem der Led-Ring angebracht wurde, war es notwendig, diesen in Verbindung mit dem Device nochmals auf seine Funktionsfähigkeit zu testen, auch wenn die Daten so im Dashboard ankommen. Hier gab es einige Schwierigkeiten, da das Dashboard nicht mit Daten versorgt werden konnte. Dies lag jedoch nicht an unserem Device, nach Rücksprache mit unserem Dozenten, lag dies an dem LoRaWAN-Gateway, welches für einen bestimmten Zeitraum außer Betrieb war.

\pagebreak
## Anforderungen

Die Anforderungen an den geplanten Sensor waren hierbei folgende:
 
* Der geplante Sensor, soll die CO2-Konzentration messen können.
* Der Sensor soll mittels Netzstecker an den Strom angeschlossen werden können.
* Das Device soll mit Adruino programmierbar sein.
* Der LED-Ring soll anhand der angegeben und gemessenen Grenzwerte die angezeigte Farbe wechseln können.
* Man soll von außen erkennen können, dass es sich um eine CO2 Ampel handelt, falls andere Studierende den Vorlesungssaal nutzen.
* Der Sensor soll Daten an ein Dashboard liefern können.
* Der Sensor soll in eine Aufputz-Box (ca. 15cmx15cm) integriert werden können.
* Die Box soll einen durchsichtigen Deckel haben, sodass man die LED-Anzeige durchscheinen sieht.
* Die Box inkl. Sensor soll leicht genug sein, um mittels Klettbändern an der Wand im Vorlesungssaal befestigt werden zu können. Sodass keine Befestigung in der Wand notwendig ist.
* Man soll den Sensor bspw. mittels kleiner Taste kalibrieren können.

\pagebreak
## Lötworkshop zur Erstellung des Sensors am 24.11.2022

Am 24.11.22 haben wir uns in einer Halle des Betriebshofs der Stadt Kehl getroffen, um die Sensorik für unser Projekt zu erstellen. Nach einer kurzen Anleitung und einem Anstecker, mit dem jeder einmal ausprobieren konnte, wie man lötet, wurden die Gehäuse für die Sensorik ausgeteilt. Im ersten Schritt musste ein Loch in die Außenwand des Gehäuses gestochen werden, sodass das Kabel für die Stromversorgung hineingelegt werden konnte. 

\begin{figure}[H]
\centering
\includegraphics[width=3in]{./images/Loetworkshop/Loet_01.jpg}
\caption{Gehäuse mit Kabel}
\end{figure} 

Danach konnten wir mit dem Löten des Sensors beginnen. Zunächst sollten die Kontakte an den Sensor angebracht werden. Dabei war zu beachten, dass sie möglichst rechtwinklig angelötet werden, damit sie später gut auf ihr Gegenstück auf dem Oktopus passen würden. Dafür haben wir eine "zweite Hand" zur Hilfe genommen.  

\begin{figure}[H]
\centering
\includegraphics[width=3in]{./images/Loetworkshop/Loet_05.jpg}
\caption{Sensor mit Kontakten in zweiter Hand}
\end{figure} 

Daraufhin haben wir die zweite Reihe der Kontakte angelötet und haben danach eine Antenne befestigt, um das Senden zu ermöglichen.

\begin{figure}[H]
\centering
\includegraphics[width=3in]{./images/Loetworkshop/Loet_08.jpg}
\caption{Sensor mit Antenne}
\end{figure} 

Den nun fertigen Sensor konnten wir auf den Oktopus stecken. Daraufhin haben wir die Brücke S9 Neopixel durchtrennt, da die zwei Board LEDs nicht parallel genutzt werden konnten. Das vorläufige Ergebnis sah aus wie folgt: 

\begin{figure}[H]
\centering
\includegraphics[width=3in]{./images/Loetworkshop/Loet_13.jpg}
\caption{Sensor am Oktopus angebracht}
\end{figure} 

Nun stellte sich aber heraus, dass für den Plan eine CO2-Ampel zu erstellen noch die LEDs fehlten, um die jeweilige CO2-Konzentration anzuzeigen. Um im Folgenden trotzdem mit der Programmierung beginnen zu können, wurde uns ein anderer, bereits fertiger Sensor geliehen, bis wir an unserem Sensor ebenfalls LED-Leuchten anbringen könnten. 

\begin{figure}[H]
\centering
\includegraphics[height=3in, angle=90]{./images/Loetworkshop/Loet18.jpg}
\caption{Ersatzsensor bei der Programmierung}
\end{figure} 

Es musste außerdem noch ein Fenster in das Gehäuse für den Sensor geschnitten werden, sodass die CO2-Ampel auch von außen gut zu sehen ist. Dafür haben wir mithilfe eines Rotationsschleifers ein Loch in die obere Seite des Gehäuses gesägt und von innen eine Plexiglasscheibe dagegen geklebt. 

\begin{figure}[H]
\centering
\includegraphics[width=3in]{./images/Loetworkshop/Loet_20.jpg}
\caption{Gehäuse mit Öffnung}
\end{figure} 

Mit dieser Plexiglasscheibe war der angebaute LED-Ring gut zu sehen. Die Entscheidung fiel auf den LED-Ring, da für einzelne LEDs nicht genügend freie Ports vorhanden waren. Deshalb haben wir ihn letztendlich am Port 0 angeschlossen. 

\begin{figure}[H]
\centering
\includegraphics[height=3in, angle=90]{./images/Loetworkshop/sensor_led_ring.jpg}
\caption{Vollständiger Sensor im Gehäuse}
\end{figure} 

Zuletzt konnten wir das fertige Gehäuse mit Klettstreifen an der Wand in der Nähe einer Steckdose anbringen, sodass auch die Stromversorgung sichergestellt war. 

\begin{figure}[H]
\centering
\includegraphics[width=3in]{./images/Loetworkshop/Standort_hs.jpg}
\caption{Klettstreifen zur Befestigung der CO2-Ampel}
\end{figure} 

\pagebreak
## Programmierung

Nachdem die einzelnen Bestandteile der Sensorik durch das Löten fest miteinander verbunden wurden und damit die eigentliche Hardware einsatzbereit ist, musste im nächsten Schritt der auszuführende Code für die Messung von CO2, Temperatur und dem PAX-Counter bereitgestellt werden. 

Dazu dient die visuelle Programmierumgebung von Arduino, welche speziell für die Verwendung mit dem Werkzeug ArduBlock entwickelt wurde.
Diese basiert auf der Blockly-Bibliothek, welche von Google entwickelt wurde, um es Benutzer  in einer besonders intuitiven Art und Weise das Programmieren zu ermöglichen. Hierbei werden visuelle Code-Blöcke, welche bereits vordefiniert sind und jeweils eine spezielle Funktion erfüllen, zusammengesetzt. Das Erlernen der Syntax einer höheren Programmiersprache ist somit nicht mehr nötig. Vielmehr verwendet Blockly eine Drag-and-Drop-Schnittstelle, bei der man visualisierte Code Einheiten aus einer Bibliothek auswählen und zusammenstellen kann.
Jeder Block repräsentiert eine bestimmte Anweisung oder Funktion, wie z.B. "Wenn-Dann" oder eine "Schleife". Die Blöcke können auch miteinander verbunden werden, um komplexere Anweisungen zu erstellen.

\begin{figure}[H]
\centering
\includegraphics[width=5in]{./images/Programmierung/Bild01.png}
\caption{ARDUINO}
\end{figure}

Die Arduino-Plattform bietet die Möglichkeit, Algorithmen in Form von grafischen Funktionsblöcken zusammenzustellen. Die Andockstellen zwischen den Blöcken sind dabei so gestaltet, dass nur passende Verbindungen zugelassen werden. Damit wird im Vorfeld bereits eine mögliche Fehlerquelle ausgeschlossen.

Um das Board über Arduino-Plattform anbinden zu können muss zuerst die richtige Schnittstelle gewählt werden. Am COM-Port muss ausgewählt werden, an welcher Serielleschnittstelle das Board angeschlossen werden soll. Für die erstmalige Einrichtung des Setup ist die letzte Nummer in der Liste auszuwählen. Danach kann das Werkzeug ArduBlock gestartet werden, mit dem die eigentliche Programmierung umgesetzt werden kann.

\begin{figure}[H]
\centering
\includegraphics[width=3in]{./images/Programmierung/Bild02.png}
\caption{Dialogfenster für Port und ArduBlock}
\end{figure}

Innerhalb des Programms wird der Sensor ausgelesen, der Messwert gespeichert und anschließend auf die serielle Schnittstelle ausgegeben. Anschließend wird mit Hilfe einer Fallunterscheidung die LED-Komponente entsprechend angesteuert.

\begin{figure}[H]
\centering
\includegraphics[width=5in]{./images/Programmierung/Bild03.png}
\caption{Programmierumgebung}
\end{figure}

Nachdem die ArduBlock Umgebung startet, wird zu Beginn mit dem ersten Block das Setup initialisiert und die Funktion Schleife zur Verfügung gestellt. Dieser erste Block bildet den Rahmen für den gesamten Code. Im Weiteren befinden sich alle Spezifikationen innerhalb dieses Rahmens.

Am linken Rand der Programmierumgebung befindet sich eine Werkzeugkiste. Diese stellt verschiedene funktionelle Blöcke bereit, wie Kontrollstrukturen, logische Operatoren, externe Interfaces und vieles mehr.

\begin{figure}[H]
\centering
\includegraphics[width=1.5in]{./images/Programmierung/Bild04.png}
\caption{Werkzeugkiste}
\end{figure}

Im ersten inhaltlichen Block wird der LED-Ring mit der Typenbezeichnung WS2812 als ansteuerbares Element eingerichtet.

\begin{figure}[H]
\centering
\includegraphics[width=5in]{./images/Programmierung/Bild05.png}
\caption{LED-Ring}
\end{figure}

Danach werden die drei Messwerte (Temperatur, Pax Counter, CO2-Gehalt der Umgebungsluft) als Schleifen definiert. Der PAX Counter dient zur Erfassung der anwesenden Personen im Raum. Hierzu sendet er ein WLAN-Signal aus und erfasst die Anzahl an WLAN-fähigen Geräten. Die Feldstärke von "-95" ist dabei so bemessen, dass der Überwachungsbereich möglichst nur den betreffenden Raum erfasst. Alle drei Messwerte werden als definierte Variablen ausgegeben:
* Temperatur in °C wird zu Temp
* Anzahl der Personen wird zu Personen
* CO2 ist als Wert der Selbe, wie die Variablenbezeichnung

\begin{figure}[H]
\centering
\includegraphics[width=5in]{./images/Programmierung/Bild06.png}
\caption{Messwerte}
\end{figure}

Darauf folgend wird durch eine “Wenn-,Dann-Operation” entsprechend der eingestellten Grenzwerte des CO2-Werts die Farbgebung der Ampel-LED angepasst.

\begin{figure}[H]
\centering
\includegraphics[width=5in]{./images/Programmierung/Bild07.png}
\caption{Wenn/Dann Operation}
\end{figure}

Im letzten Block wird die Verbindung mit dem Gateway und dem Dashboard hergestellt und die einzelnen Messwerte als definierte Variablen jeweils einem Datenfeld zugeordnet. Hierdurch wird es möglich, innerhalb des Dashboards diese Werte aufzugreifen und zu verwenden.

\begin{figure}[H]
\centering
\includegraphics[width=5in]{./images/Programmierung/InkedBild08.jpg}
\caption{Network Block}
\end{figure}

Insgesamt durchläuft die Code-Schleife immer wiederholend die Schritte Messert aufnehmen, Messwert ausgeben, Ampelfarbe gemäß des CO2 Messwerts anpassen, 1000 ms Wartezeit und Übermittlung der Daten ans Gateway.
Im Anschluss muss der Code nur noch per USB-Schnittstelle auf den Microcontroller hochgeladen werden.

\pagebreak
# Abbildung der Daten im Dashboard

## Darstellung der Daten

CO2-Werte werden üblicherweise in Form von Linien- oder Balkendiagrammen dargestellt, um Veränderungen im Zeitverlauf aufzuzeigen. Wir haben uns hier für ein Liniendiagramm entschieden. In dieser Grafik wird der CO2-Gehalt in Teilen pro Million (ppm) auf der Y-Achse dargestellt, während die Zeit auf der X-Achse aufgetragen wird. Jeder Punkt im Diagramm repräsentiert eine Messung zu einem bestimmten Zeitpunkt. So kann man leicht Muster und Trends in den CO2-Werten erkennen und Veränderungen im Zeitverlauf verfolgen. Außerdem kann man leicht sehen, wann die CO2-Konzentration über einen bestimmten Schwellenwert hinausgeht, was ein Indikator für unzureichende Luftzirkulation oder Überbelegung eines Raumes sein kann.

## Test Datensammlung in Vorlesungsathmosphäre

Ziel:
Die CO2-Ampel sollte in einem Vorlesungsraum getestet werden und die Konzentration von CO2 in der Luft über einen bestimmten Zeitraum gemessen werden. Die Daten sollten grafisch im Dashboard dargestellt werden, um Veränderungen der Konzentration von CO2 im Raum über die Zeit zu beobachten. Ebenso sollte getestet werden, ob der LED-Ring bei den festgelegten Schwellenwerten die Farbe ändert.

Durchführung:
Der Sensor wurde um 11:15 eingesteckt und leuchtete direkt grün. Um vorab zu testen, ob der Sensor Daten misst und diese überträgt, wurde er kurz angepustet um die CO2-Konzentration zu erhöhen. Der LED-Ring leuchtete rot und im Dashboard konnte ein Anstieg der Kurve sowie eine Veränderung der Temperatur beobachtet werden.
Es wurde gelüftet, um den CO2-Wert wieder an die Außenluft anzugleichen. Nach weniger als zwei Minuten leuchtete der Ring wieder grün. Die Fenster blieben zehn Minuten geöffnet und in diesem Zeitraum konnte beobachtet werden, wie der CO2-Wert im Raum sich bei 400 ppm einpendelte.

\begin{figure}[H]
\centering
\includegraphics[width=3in]{./images/ScreenshotsDashboard/screenshot1.png}
\caption{Diagramm CO2-Werte zum Startzeitpunkt}
\end{figure} 

Nachdem die Fenster wieder geschlossen waren, arbeiteten wir wie gewohnt. Da wir nur zu zweit in dem großen Raum waren, setzten wir uns in die Nähe des Sensors, um bestmöglich eine Vorlesungsstunde mit gefülltem Vorlesungsraum zu simulieren.
Der CO2-Wert stieg an und erreichte nach etwa 20 Minuten wieder den roten Schwellenwert. Es ist zu beachten, dass der stufenhafte Anstieg in einer tatsächlichen Vorlesung womöglich etwas flacher ablaufen würde und dies nur unser Versuch war, einen vollen Raum nachzustellen.

\begin{figure}[H]
\centering
\includegraphics[width=3in]{./images/ScreenshotsDashboard/screenshot2.png}
\caption{Diagramm CO2-Werte Anstieg}
\end{figure} 

Nachdem die Fenster geöffnet wurden, hatte sich nach ungefähr drei Minuten der CO2-Wert wieder bei 400 ppm eingependelt.

\begin{figure}[H]
\centering
\includegraphics[width=3in]{./images/ScreenshotsDashboard/screenshot3.png}
\caption{Diagramm CO2-Werte nach dem Lüften}
\end{figure} 

Ebenfalls kann der Sensor die Temperatur messen sowie die Anzahl der Wifi Geräte in der Umgebung. Diese Werte werden zusätzlich im Dashboard dargestellt.

\begin{figure}[H]
\centering
\includegraphics[width=3in]{./images/ScreenshotsDashboard/screenshot4.png}
\caption{Gesamtes Dashboard}
\end{figure} 

## Ergebnisse

Der Test mit der CO2-Ampel hat gezeigt, dass der Sensor funktioniert und Daten erfassen und übertragen kann. Die Daten wurden grafisch dargestellt, was es ermöglicht hat, Veränderungen der CO2-Werte in Abhängigkeit zum Zeitverlauf zu verfolgen. Außerdem konnte demonstriert werden, wie sich der CO2-Gehalt ändert, wenn Fester geöffnet und geschlossen werden.

Obwohl die Datensammlung nur ein Versuch war, eine Vorlesungssituation nachzustellen, kann das Verhalten einer CO2-Ampel im Vorlesungsraum abgeleitet werden. Der stufenweise Anstieg und die kurze Zeitspanne von 20 Minuten, bis die Ampel rot wurde, ist möglicherweise nicht repräsentativ, aber trotzdem ein guter Indikator für das Verhalten der Ampel im Einsatz.

Zusammenfassend lässt sich sagen, dass die Test-Datensammlung erfolgreich war und gezeigt hat, dass die CO2-Ampel ein nützliches Instrument sein kann, um den CO2-Gehalt in Innenräumen zu überwachen und sicherzustellen, dass die Luftqualität ausreichend ist.

\begin{figure}[H]
\centering
\includegraphics[width=3in]{./images/ScreenshotsDashboard/ampel_dashboard.jpeg}
\caption{Darstellung CO2-Ampel mit Dashboard}
\end{figure} 
  
\pagebreak    
# Fazit

Bei dem Projekt der CO2-Ampel ist es uns gelungen, einen funktionsfähigen Sensor zu entwickeln, der die CO2-Konzentration in einem Raum misst und darstellt. Die Verwendung von Sensortechnologie und Programmierung ermöglicht es, genaue Messungen durchzuführen und die Ergebnisse auf einfache und verständliche Weise anzuzeigen. Die Implementierung der Ampel kann zur besseren Überwachung und Anpassung der Innenraumluftqualität im Vorlesungsraum beitragen. Die vollständige Inbetriebnahme und Installation an dem vorgesehenen Anbringungsort war erfolgreich.
Allerdings gab es einige Probleme während des Projekts. Die Abhängigkeit von Anwesenden im Vorlesungsraum zur Datenerhebung erschwerte die Datensammlung im Dashboard. Die Verwendung von GitLab war ambivalent, da es zwar sinnvoll für das Sammeln von ersten Erfahrungen für eine mögliche spätere professionelle Zusammenarbeit mit Entwicklern und Softwareanbietern war, aber wir Zeit brauchten uns in Markdown mit LaTex als Dokumentationssprache einzuarbeiten.
Das Projekt hat jedoch auch viele Vorteile für das spätere Berufsleben gebracht, wie beispielsweise die ersten Erfahrungen mit Markdown und konkrete Umsetzungsideen für mögliche SmartCity-Projekte. Das Verständnis für verschiedene LoRaWAN-Anwendungen wurde verbessert. Sehr dankbar sind wir für die gute Betreuung des Dozierenden, der das Projekt hilfsbereit unterstützte.
Für die Zukunft gibt es Potenzial, die Informierung und Kommunikation mit den Nutzenden des Vorlesungsraumes zu verbessern und das Dashboard auf der Webseite oder der App zu integrieren. Die kommenden DVM-Studierenden haben die Möglichkeit, das Projekt weiter auszubauen, zu erweitern und zu verbessern.

\pagebreak
# Literaturverzeichnis

<!--
## Referenzen
-->
